<?php

class codeLog
{
    private $handle;

    public function __construct($filename)
    {
        $this->handle = fopen(DIR_LOGS . $filename, 'a');
    }

    public function write($message)
    {
        $date = new DateTime("now", new DateTimeZone('America/Sao_Paulo'));
        fwrite($this->handle, $date->format('d/m/Y H:i:s') . ' - ' . print_r($message, true) . "\n");
    }

    public function __destruct()
    {
        fclose($this->handle);
    }
}